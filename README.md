# Task 

## Points 
- [x] Pull last 3 years' data of SBIN.NS from yahoo finance
- [x] If the previous week average price is less than this week average price, send {'SBIN':'BUY'}, else send {'SBIN':'SELL'}
- [ ] Insert a row in PostgreSQL with these fields Run Date, Symbol, Recommendation


## Setup
Clone the git clone repository on your local machine and run

`pip install -r requirements.txt`

in the same folder. (You can create a virtual env before if you want)

then 

`cd ./yfin_rest` and run `python manage.py runserver` and visit your localhost to see the api running.

## API endpoints 

- `localhost:port/yahoo/fetch-history` with parmas `ticker`[required : stock name] , `start` [optional], `end` [optional], `interval` [optional] 
    - example usage: `http://127.0.0.1:8000/yahoo/fetch-history?ticker=SBIN.NS` default values for start, end and interval are set such that they will fetch last three year data.
- `localhost:port/yahoo/compare-prices` with params `ticker` [required : stock name] 
    - example usage: `http://127.0.0.1:8000/yahoo/compare-prices?ticker=SBIN.NS` returns {'SBIN':'BUY'} If the previous week average price is less than this week average price else {'SBIN':'SELL'}
## Screenshots
Fetch History
![Alt text](/screenshots/endpoint1.png?raw=true "Optional Title")
Compare Weekly Prices
![Alt text](/screenshots/endpoint2.png?raw=true "Optional Title")
