import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

BASE_URL = 'https://query2.finance.yahoo.com'
headers = user_agent_headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 \
    (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
}
date_pattern = "%d-%m-%Y"


def get_todays_date_string():
    return datetime.now().strftime(date_pattern)


def convert_current_time_to_epoch():
    int(time.mktime(time.strptime(datetime.now().strftime(date_pattern), date_pattern)))


def convert_to_epoch(time_string):
    return int(time.mktime(time.strptime(time_string, date_pattern)))


def url_builder(endpoint, ticker):
    url = f'{BASE_URL}/v7/{endpoint}/{ticker}'
    return url


def get_default_params(years=3):
    today = datetime.now().strftime(date_pattern)
    today_years_ago = (
        datetime.now() - relativedelta(years=years)).strftime(date_pattern)
    return (today_years_ago, today)


def get_week_range(week_end_date):
    last_day = datetime.strptime(week_end_date, date_pattern)
    first_day = (last_day - relativedelta(days=7)).strftime(date_pattern)

    last_day = convert_to_epoch(week_end_date)
    first_day = convert_to_epoch(first_day)
    return (first_day, last_day)


def convert_epoch_to_date_string(epoch_date):
    return time.strftime(date_pattern, time.localtime(epoch_date))


def compute_average(iterable):
    avergage = sum(iterable)/len(iterable)
    return avergage
