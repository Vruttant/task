from django.urls import path, include
from .views import FetchHistoryView, CompareWeekPricesView

urlpatterns = [
    path('fetch-history', FetchHistoryView.as_view()),
    path('compare-prices', CompareWeekPricesView.as_view())
]
