from datetime import date
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import requests
from .utils import *


class FetchHistoryView(APIView):
    def get(self, request, format=None):

        # retrieve queries passed from the get requests
        date_today = convert_current_time_to_epoch()
        stock_name = request.GET.get('ticker', None)
        start = request.GET.get('start', date_today)
        end = request.GET.get('end', date_today)
        interval = request.GET.get('interval', '1d')

        # handling default date values
        # default values pull last 3 years history
        # of the given stock.
        if start is None and end is None:
            start, end = get_default_params()

        # converting passed date values to
        # epoch time since that's what the api supports.
        start = convert_to_epoch(start)
        end = convert_to_epoch(end)

        # setting parameters for calling the yahoo finance API.
        paramters = {
            'period1': start,
            'period2': end,
            'interval': interval
        }

        # builds a callable yfinance url which can
        # be called with given params
        url = url_builder('finance/chart', stock_name)

        data = requests.get(url=url, params=paramters, headers=headers)
        return Response(data.json(), status=status.HTTP_200_OK)


class CompareWeekPricesView(APIView):
    def get(self, request, format=None):
        stock_name = request.GET.get('ticker', None)

        current_week_start, current_week_end = get_week_range(
            get_todays_date_string())

        last_week_start, last_week_end = get_week_range(
            convert_epoch_to_date_string(current_week_start))

        url = url_builder('finance/chart', stock_name)
        params_current_week = {
            'period1': current_week_start,
            'period2': current_week_end,
            'interval': '1d',
        }

        params_last_week = {
            'period1': last_week_start,
            'period2': last_week_end,
            'interval': '1d',
        }

        data_current_week_response = requests.get(
            url=url, params=params_current_week, headers=headers)

        data_last_week_response = requests.get(
            url=url, params=params_last_week, headers=headers
        )
        data_current_week_response = data_current_week_response.json()
        data_last_week_response = data_last_week_response.json()
        last_week_average = compute_average(data_last_week_response.get(
            'chart').get('result')[0]['indicators']['quote'][0]['high'])
        current_week_average = compute_average(data_current_week_response.get(
            'chart').get('result')[0]['indicators']['quote'][0]['high'])

        if last_week_average < current_week_average:
            comparision_response = {
                str(stock_name): 'BUY',
            }
        else:
            comparision_response = {
                str(stock_name): 'SELL',
            }
        # average_last_week = data_last_week_response.json().get(
        #     'indicators').get('quote').get('high')

        # print(average_last_week)
        print(last_week_average)
        print(current_week_average)
        # data_last_week_response.get('')
        # print(data_current_week)
        # print(data_last_week)
        # data = data_current_week + data_last_week

        return Response(comparision_response, status=status.HTTP_200_OK)
