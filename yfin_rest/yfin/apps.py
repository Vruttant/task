from django.apps import AppConfig


class YfinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'yfin'
